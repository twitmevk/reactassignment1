import React from 'react'
import '../bootstrap.css'
import '../App.css'

class ProductForm extends React.Component {

    render() {
        return (
            <div className="container">
                <form className="form-horizontal" id="productCreate" onSubmit={this.props.submitHandler}>
                    <h1 id='title'>Add/Edit a Product</h1>
                    <div className="form-group">
                        <label htmlFor="inputName" className="col-sm-2 control-label">Product Name: *</label>
                        <div className="col-sm-6">
                            <input type="text" name="name" id="inputName" className="form-control"
                                value={this.props.newName}
                                onChange={this.props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputCategory" className="col-sm-2 control-label">Category: *</label>
                        <div className="col-sm-6">
                            <select className="form-control" name="category"
                                value={this.props.newCategory}
                                onChange={this.props.changeHandler}
                            >
                                <option value="">--SELECT--</option>
                                <option value="cat1">cat1</option>
                                <option value="cat2">cat2</option>
                                <option value="cat3">cat3</option>
                                <option value="cat4">cat4</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputDescription" className="col-sm-2 control-label">Description:</label>
                        <div className="col-sm-6">
                            <input type="text" name="description" id="inputDescription"
                                className="form-control"
                                value={this.props.newDescription}
                                onChange={this.props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputPrice" className="col-sm-2 control-label">Price: *</label>
                        <div className="col-sm-6">
                            <input type="text" name="price" id="inputPrice"
                                className="form-control"
                                value={this.props.newPrice}
                                onChange={this.props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-6">
                            <p className="alert-danger" id="myErrors">{this.props.errormessage}</p>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-6">
                            <button type="submit" className="btn btn-success pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div >
        )
    }
}

export default ProductForm