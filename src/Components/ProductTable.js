import React from 'react'

class ProductTable extends React.Component {

    renderTableData() {
        return this.props.products.map((product, index) => {
            if (product) {
                const { name, category, description, price } = product
                return (
                    <tr key={name + '-' + index}>
                        <td>{name}</td>
                        <td>{category}</td>
                        <td>{description}</td>
                        <td>{price}</td>
                        <td>
                            <button className="btn btn-primary"
                                onClick={() => {
                                    this.props.rowEditHandler(index)
                                }}>Edit</button>&nbsp;
                            <button className="btn btn-danger"
                                onClick={() => {
                                    this.props.rowDeleteHandler(index)
                                }}>Delete</button>
                        </td>
                    </tr>
                )
            }
        })
    }

    render() {
        return (
            <div className="container">
                <h1 id='title'>Products List</h1>
                <table id='products' className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Edit/Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ProductTable