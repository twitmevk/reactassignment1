import React from 'react'

import ProductForm from './ProductForm'
import ProductTable from './ProductTable'

export class Wrapper extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            name: '',
            category: '',
            description: '',
            price: '',
            type: 'submit',
            id: '',
            products: [],
            errormessage: ''
        }
    }

    submitHandler = (e) => {
        e.preventDefault()

        let err = ''
        if (this.state.name === "") {
            err += 'Name Missing\n';
        }
        if (this.state.category === "") {
            err += 'Product must have a category\n'
        }
        if (this.state.price === "") {
            err += 'You must enter a price\n'
        } else if (!Number(this.state.price)) {
            err += 'Price is not valid'
        }

        if (err) {
            alert(err)
            return false
        }

        let products = [...this.state.products]

        if (this.state.type === 'submit') {
            products.push({
                name: this.state.name,
                category: this.state.category,
                description: this.state.description,
                price: this.state.price,
            })
        } else if (this.state.type === 'save') {
            const id = this.state.id
            products[id].name = this.state.name
            products[id].category = this.state.category
            products[id].description = this.state.description
            products[id].price = this.state.price
        }

        this.setState({
            products,
            name: '',
            category: '',
            description: '',
            price: '',
            type: 'submit'
        })

    }

    changeHandler = (event) => {
        let nam = event.target.name
        let val = event.target.value
        let err = ''
        if (nam === "name" && val === "") {
            err = <strong>Name Missing</strong>
        } else if (nam === "category" && val === "") {
            err = <strong>Product must have a category</strong>
        } else if (nam === "price" && val === "") {
            err = <strong>You must enter a price</strong>
        } else if (nam === "price" && !Number(val)) {
            err = <strong>Not a valid price</strong>
        } else {
            err = ''
        }
        this.setState({ errormessage: err })
        this.setState({ [nam]: val })
    }

    rowEditHandler = (id) => {
        let products = [...this.state.products]

        const { name, category, description, price } = products[id]
        this.setState({
            products,
            name,
            category,
            description,
            price,
            type: 'save',
            id
        })
    }

    rowDeleteHandler = (id) => {
        let products = [...this.state.products]

        delete products[id]

        console.log(products)
        this.setState({
            products,
            name: '',
            category: '',
            description: '',
            price: '',
            type: 'submit'
        })
    }

    render() {
        return (
            <div id="root">
                <ProductForm
                    submitHandler={this.submitHandler}
                    changeHandler={this.changeHandler}
                    newName={this.state.name}
                    newCategory={this.state.category}
                    newDescription={this.state.description}
                    newPrice={this.state.price}
                    errormessage={this.state.errormessage}
                />
                <ProductTable
                    products={this.state.products}
                    rowDeleteHandler={this.rowDeleteHandler}
                    rowEditHandler={this.rowEditHandler}
                />
            </div>
        )
    }
}

export default Wrapper
